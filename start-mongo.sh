#!/bin/bash

# Mongo was intalled as systemctl service (if you want to use it a a system service)
# sudo systemctl start mongod

HOME_OTP_IMPROVER="$(pwd)"
 
echo "starting mongodb at $HOME_OTP_IMPROVER/data/"

mongod --dbpath $HOME_OTP_IMPROVER/data/

