//
//
//
var mongoose = require('mongoose');
//
var UserSchema = new mongoose.Schema({
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    username: {
      type: String,
      unique: true,
      required: true,
      trim: true
    },
    password: {
      type: String,
      required: true,
    },
    passwordConf: {
      type: String,
      required: true,
    },
    otp_secret:{
        type: String,
        unique: true,
        trim: true          
    },
    otp_active:{
        type: Boolean,
        required:true       
    }
});
//
var User = mongoose.model('User', UserSchema);
module.exports = User;
//