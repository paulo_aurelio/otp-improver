var express = require('express');
var router = express.Router();

/* GET users listing. */
/* GET Userlist page */
router.get('/', function(req, res){
    //
    var db = require("../db");    
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
    Users.find({}).lean().exec(
        function (e, docs) {
            res.render('userlist', {"userlist": docs});
    });
    //

});

/* GET New User page. */
router.get('/new', function(req, res) {
    res.render('newuser', { title: 'Add New User' });
});

/* POST to Add User Service */
router.post('/add', function (req, res) {

    var db = require("../db");
    var userName = req.body.inputUserame;
    var userEmail = req.body.inputEmail;

    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
    var user = new Users({ name: userName, email: userEmail });
    user.save(function (err) {
        if (err) {
            console.log("Error! " + err.message);
            return err;
        }
        else {
            console.log("Post saved");
            var userNameToFind = encodeURIComponent(user.name);
            res.redirect("/otp/?usr=" + userNameToFind);
        }
    });
});

module.exports = router;
