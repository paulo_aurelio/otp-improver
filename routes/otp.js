var express = require('express');
var router = express.Router();

//var otplib =  require('otplib');
var authenticator = require('otplib/authenticator');
var qr = require('qr-image');
var crypto = require('crypto');

//setting crypto when declare otplib/authenticator especifically
authenticator.options = {
  step: 30,
  window: 1,
  crypto: crypto
};

/* Criar a pagina que vai portar o qrCode */
router.get('/', (req, res) =>  {

  var userNameToFind = req.query.usr;

  //find User By userName
  var db = require("../db");
  var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
  Users.findOne({name: userNameToFind}).exec(function(err, user) {
      console.log('user: ' + user);
      if (!user.secret_verified) {
          // const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';

          // Alternatively: const secret = otplib.authenticator.generateSecret();
          //const secret = authenticator.generateSecret();
          user.secret =  authenticator.generateSecret(); ;
          //user.secret = secret;
          user.save();
          //
          res.render('otp', {user: user});
      } else {
          res.redirect('/users');
      }
      //
  });
     
});

/* Pagina criada chama esta url para gerar o QRCode*/
router.get('/qr', (req, res) =>  {

  var userName = req.query.n;
  var service = req.query.m;

  //find User By userName
  var db = require("../db");
  var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
  Users.findOne({name: userName}).exec(function(err, user) {
      console.log('user: ' + user);
      //
      console.log("/qr -> secret: " + user.secret);
      //console.log("/qr -> token: " + token);
      //console.log("/qr -> isValid: " + isValid);

      //const secret = 'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';
      const otpauth = authenticator.keyuri(user.name, service, user.secret);
      console.log("otpauth: " + otpauth);

      const qrCodeImage = qr.image(otpauth, {type: 'png'});
      res.type('png');
      qrCodeImage.pipe(res);
      //
  });

});

router.post('/qr/verify', function (req, res) {

  //const token = authenticator.generate(secret);
  // Este token será gerado pelo app de Token que scaneou o QRCode com a Secret!
  var token = req.body.inputOTPCode;
  var userName = req.body.username;

  // Validar o Token Enviado! 

  //find User By userName
  var db = require("../db");
  var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
  Users.findOne({name: userName}).exec(function(err, user) {
      console.log('user: ' + user);
      //
          //const isValid = otplib.authenticator.check(token, secret);
      // or

      const secret = user.secret; //'KVKFKRCPNZQUYMLXOVYDSQKJKZDTSRLD';
      const isValid = authenticator.verify({ token, secret });

      console.log("/qr/verify -> token: " + token);
      console.log("/qr/verify -> user.ecret: " + user.secret);
      console.log("/qr/verify -> secret: " + secret);
      console.log("/qr/verify -> isValid: " + isValid);

      // se a verificação do secret foi sucesso cadastro.
      if ( isValid && !user.secret_verified ){
          user.secret_verified = true;
          user.save();
          // 
          res.redirect('/users');
          
      } else {
        res.render('index', { auth: isValid });
      }       
  });
  //
});

/* Criar a pagina que vai Testar o 2FA */
router.get('/use', (req, res) =>  {

    var userNameToFind = req.query.usr;
  
    //find User By userName
    var db = require("../db");
    var Users = db.Mongoose.model('usercollection', db.UserSchema, 'usercollection');
    Users.findOne({name: userNameToFind}).exec(function(err, user) {
        console.log('user: ' + user);
        if (user.secret_verified) {
            //
            res.render('otp', {user: user});
        } else {
            res.redirect('/users');
        }
        //
    });
       
});
  

module.exports = router;
